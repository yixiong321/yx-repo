var root =document.body

//Advantage of states is that can pass inside views
const state = {
    //this is for assigning id to blog posts
    count:0,
    //this is to control whether user can make new form
    addPostState:true,
    FieldEmpty:true
}
const actions = {
    increment: ()=>{
        state.count+=1
    },
    decrement:()=>{
        state.count-=1
    },
    disable: ()=>{
        state.addPostState=false
    },
    enable:()=>{
        state.addPostState=true
    },
    isNotEmpty:()=>{
        state.FieldEmpty=false;
    },
    isEmpty:()=>{
        state.FieldEmpty=true
    },
}



//Components
var Hello = {
    oninit:function(){
        console.log("Initializing for hello")
    },
    oncreate:function(){
        console.log('creating')
    },
    onupdate:function(){
        //closure
        var counter=0
        return function(){
            counter+=1
            //console.log("number of times updated(hello): "+counter)
        }
    }(),
    view:function(){
        return m("new",{class:"new"},[
            m("h1",{class:"title"},"Hello world!"),
            //the #!/ means anything after it is a route path
            m("li",[m("a",{href:"#!/Main"},"View Current Entries"),]),            
            m("button",
                {
                    id:"b1",
                    onclick: () => {
                        testform()
                        actions.disable
                    },
                },
                    "Click to add new entry!"),
            m("br"),
            m("newform",newForm),
            m("br")
            ])
    }
}

//closure component with state
function Main() {
    var c=0;
    function update() {
        return c=blogDB.length
    }
    return{
    view:function(){
        return m("entries",[
            m("h1",{id:"CurrentEntries"},"Current Entries:"),
            m("h1","Total number of entries:" +update()),
            m("curView",CurrentDBView)
        ])
    }}
}

var newForm= []
var CurrentDBView=[]
var blogDB=[]

/*
m("div", {id: "box"}, "hello")
// renders to this HTML:
// <div id="box">hello</div>
*/

//this function is to insert a new form(with all the DOMS) into the newForm array that is to be displayed
function testform(){
    if(state.addPostState){
        actions.disable(),
        newForm.push(
            m("form",[
                m("label",{class:"label",for:"name"},"Name"),m("br"),
                m("input",{class:"Input",type:"text",
                    placeholder:"Name",id:"name"}),
                m("br"),
                m("label",{class:"label",for:"title"},"Title"),m("br"),
                m("input",{class:"Input",type:"text",
                    placeholder:"Title",id:"title"}),
                m("br"),
                m("label",{class:"label",for:"content"},"Content:"),m("br"),
                m("input",{class:"Input",type:"text",
                    placeholder:"Content",id:"content"}),
                m("br"),
                m("label",{class:"label",for:"date"},"Date:"),m("br"),           
                m("input",{class:"Input",type:"text",
                    placeholder:"Date",id:"date"}),
                m("br"),
                m("button",{class:"Post",
                    type:"button",onclick:()=>{
                        gotEmptyField()
                        if(state.FieldEmpty){
                            //display warning
                            console.log("Please enter all field")
                        }
                        else{
                            actions.increment(),
                            addBlogPost(state.count),
                            actions.enable()
                        }
                        
                    }},"Post"),
                m("button",{class:"Cancel",
                    type:"button",onclick:()=>{newForm=[]}},"Cancel"),
            ]
        ))
}}

function gotEmptyField(){
   if (document.getElementById("name").value.length!=0){
        actions.isNotEmpty()
   }
   if (document.getElementById("title").value.length!=0){
        actions.isNotEmpty()
   }
   if (document.getElementById("content").value.length!=0){
        actions.isNotEmpty()   
   }
   if (document.getElementById("date").value.length!=0){
        actions.isNotEmpty()
    }
}

//To create a dictionary for the new entry and then push to DB be4 displaying it
function addBlogPost(idFromState) {
    //create a dictionary
    new_entry = {
        "name":document.getElementById("name").value,
        "title":document.getElementById("title").value,
        "content":document.getElementById("content").value,
        "date":document.getElementById("date").value,
        "id":idFromState
    }
    //add to array of dictionaries
    blogDB.push(new_entry)
    
    displayCurDB()
    newForm=[]
}

//this is to iterate thru the DB and generate the DOMS for
//each entry and its values be4 pushing to the array to be displayed
function displayCurDB(){
    CurrentDBView=[]
    for(let i=0;i<blogDB.length;i++){
        CurrentDBView.push(m("div",{id:blogDB[i].id}, [
            m("label",{class:"blog",for:"name"},"Name:"),
            m("h3",{class:"blog",},blogDB[i].name),
            m("label",{class:"blog",for:"title"},"Title:"),
            m("h3",{class:"blog"},blogDB[i].title),
            m("label",{class:"blog",for:"content"},"Content:"),
            m("h3",{class:"blog"},blogDB[i].content),
            m("label",{class:"blog",for:"date"},"Date:"),
            m("h3",{class:"blog"},blogDB[i].date),
            m("button",{class:"blog",
                onclick:()=>{editBlogPost(i)}},"Edit"),
            m("button",{class:"blog",onclick:()=>{
                removeBlogPost(i)
                }},"Delete")
        ]))
    }
}

function removeBlogPost(index){
    blogDB.splice(index,1)
    actions.decrement()
    /*console.log("index:"+index)
    console.log(blogDB)*/
    displayCurDB()
}

function editBlogPost(index){
    CurrentDBView=[]
    //need to use let to make i local
    for(let i=0;i<blogDB.length;i++){
        if (index==i){
            CurrentDBView.push(m("div",[
                m("input",{
                    class:"editblog",
                    type:"text",
                    placeholder:"Name",
                    value:blogDB[i].name,
                    id:"name"
                    }),
                m("br"),
                m("input",{
                    class:"editblog",
                    type:"text",
                    placeholder:"Title: ",
                    value:blogDB[i].title,
                    id:"title"}),
                m("br"),
                m("input",{
                    class:"editblog",
                    type:"text",
                    placeholder:"Content: ",
                    value:blogDB[i].content,id:"content"
                }),
                m("br"),
                m("input",{
                    class:"editblog",
                    type:"text",
                    placeholder:"Date: ",
                    value:blogDB[i].date,id:"date"}),
                m("br"),
                m("button",{class:"button",onclick:()=>{saveEntry(i)}},"Save"),
                //cannot just pass in i so saveEntry due to closure
            ]))            
        }else{
            CurrentDBView.push( m("div",{class:"blog"},[
                m("h3",{class:"blog"},blogDB[i].name),
                m("h3",{class:"blog"},blogDB[i].title),
                m("h3",{class:"blog"},blogDB[i].content),
                m("h4",{class:"blog"},blogDB[i].date),
            ]))
        }
    }
}

function saveEntry(entryToDelete){
    edited_entry = {
        "name":document.getElementById("name").value,
        "title":document.getElementById("title").value,
        "content":document.getElementById("content").value,
        "date":document.getElementById("date").value,
    }
    console.log(entryToDelete)
    blogDB.splice(entryToDelete,1,edited_entry)
    console.log(blogDB)
    displayCurDB()
}

m.route(root,"/Hello",{
    "/Hello": Hello,
    "/Main": Main,}
)

/*
//Passing data to components
var gg = {
    view:function(vnode){
        return m("div",vnode.attrs.value+" yx")
    }
}
var Example = {
    view: function (vnode) {
        return m(gg, {value:"Hello, " +"I "+ vnode.attrs.value})
    }
}
//consume it
var Test = {
    view:function(){
        return m(Example, {value: "am "})
    }
}
*/

